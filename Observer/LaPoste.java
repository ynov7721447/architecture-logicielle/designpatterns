package Observer;

import java.util.HashSet;
import java.util.Iterator;

public class LaPoste {
    HashSet<BoiteALettre> abonnes = new HashSet<>();


    public boolean unsubscribe(BoiteALettre boiteALettre) {
        abonnes.remove(boiteALettre);
        return true;
    }

    public void subscribe(BoiteALettre boiteALettre) {
        abonnes.add(boiteALettre);
    }

    public void publish(String message) {

        for (BoiteALettre abonne : abonnes) {
            System.out.println(message);
        }

    }
}
